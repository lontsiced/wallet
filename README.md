# wallet

Given a Wallet containing Stocks, build a function that compute the value of wallet in a currency.

The Stocks have a quantity and a StockType. The StockType can be for example petroleum, Euros, bitcoins and Dollars.

## Getting started

windows :
 - Se placer dans le dossier projet:
   * npm install: installation des dependances
   * ng serve: démarrage de l'app sur le port par défaut
- ouvrir le navigateur sur local http://localhost:4200/

