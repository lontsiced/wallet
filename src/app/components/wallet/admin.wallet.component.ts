import {Component, OnInit, ViewChild} from '@angular/core';
import {Stock, StockCompute} from "../../interface/stock.interface";
import {FormControl} from "@angular/forms";
import {RateProviderService} from "../../service/rate-provider.service";
import {Wallet} from "../../model/wallet.model";

/**
 * @title Basic use of the tab group
 */
@Component({
    selector: 'app-wallet',
    templateUrl: './admin.wallet.component.html',
    styleUrls: ['./admin.wallet.component.css'],
})
export class AdminWalletComponent implements OnInit {

    wallets: StockCompute [] = [];

    myControlStockType = new FormControl();
    myControlQuantity = new FormControl();
    myControlCurrency = new FormControl();

    // Injection du service
    constructor(
        private rateProviderService: RateProviderService
    ) {
    }

    ngOnInit(): void {
    }



    async compute_value() {
        // Initialisation d'un wallet avec les valeurs de L'UI
        let stock: Stock = (
            {
                stockType: this.myControlStockType.value,
                nberStockType: +this.myControlQuantity.value
            }
        );

        let wallet: Wallet = new Wallet(stock)

        //Compute value
        wallet.value(this.myControlCurrency.value, this.rateProviderService)
            .subscribe( reponse => {
                console.log(reponse)
                if(reponse['success'] == true){
                    this.wallets.push(
                        {
                            stock: stock,
                            currency: this.myControlCurrency.value,
                            value:(+reponse.rates[this.myControlCurrency.value]) * stock.nberStockType
                        }
                    );
                }else {
                    alert(reponse['error']['code']+': ' + reponse['error']['type'])
                }

                },
                error => console.log('Non trouvé: '),
                () => console.log('finaly')
            );
    }


    removeToWallet(i: number) {
        this.wallets.splice(i, 1);
    }

}
