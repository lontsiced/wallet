import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {


  constructor(
      private http: HttpClient,
  ) { }


  // Get http api
  get(baseUrl: string,stockType: string, currency: string): Observable<any> {
    return this.http.get(`${baseUrl}&base=${stockType}&symbols=${currency}`);
  }

}
// http://data.fixer.io/api/latest?access_key=b32b54acdcae41a43cc67cf8ecbab50b&base=EUR&symbols=USD
