import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {ApiService} from "./api.service";
import {HttpResp} from "../interface/response.interface";
import {RateProvider} from "../interface/rateprovider.interface";

@Injectable({
  providedIn: 'root'
})
export class RateProviderService implements RateProvider{

  baseUrl = 'http://data.fixer.io/api/latest'
  access_key =  'b32b54acdcae41a43cc67cf8ecbab50b'

  constructor(
      private apiService: ApiService
  ) { }


  rate(stockType: string, currency: string): Observable<any> {
    return this.apiService.get(`${this.baseUrl}?access_key=${this.access_key}`, stockType, currency);
  }


  //compute currency: http://data.fixer.io/api/latest?access_key=b32b54acdcae41a43cc67cf8ecbab50b&base=EUR&symbols=USD
}
