import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {LOCALE_ID, CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import {APP_BASE_HREF, DatePipe} from '@angular/common';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDateFormats, MatNativeDateModule} from '@angular/material/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MglTimelineModule} from 'angular-mgl-timeline';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {ApiService} from './service/api.service';
import {AdminWalletComponent} from './components/wallet/admin.wallet.component';
import {MatTooltipModule} from "@angular/material/tooltip";
registerLocaleData(localeFr);


export const MY_FORMAT: MatDateFormats = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'DD/MM/YYYY',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@NgModule({
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
        AppRoutingModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatRadioModule,
        MatNativeDateModule,
        MatSelectModule,
        NgbModule,
        BrowserAnimationsModule,
        MglTimelineModule,
        MatTooltipModule,
    ],
    declarations: [
        AppComponent,
        AdminWalletComponent,
    ],
    providers: [
        DatePipe,
        ApiService,
        {provide: LOCALE_ID, useValue: 'fr' },
        { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMAT },
        {provide: APP_BASE_HREF, useValue: ''}
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
