export interface Stock {
  nberStockType : number;
  stockType: string;
}

export interface StockCompute {
  stock: Stock;
  currency: string;
  value: number
}
