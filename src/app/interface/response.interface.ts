export interface HttpResp {
  base: string;
  date: string;
  rates: {}
  success: boolean;
  timestamp: number;
  prototype: any
}
