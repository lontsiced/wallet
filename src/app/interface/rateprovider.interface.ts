import {Observable} from "rxjs/Observable";
import {HttpResp} from "./response.interface";

export interface RateProvider {
    rate (stockType: string, currency:string) : Observable<HttpResp>
}