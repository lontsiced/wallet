import {Stock} from "../interface/stock.interface";
import {RateProviderService} from "../service/rate-provider.service";
import {Observable} from "rxjs/Observable";
import {HttpResp} from "../interface/response.interface";

export class Wallet{
    stock: Stock;


    constructor(
        stock
    ) {
        this.stock = stock;
    }

    public value(currency: string,rateProviderService: RateProviderService):Observable<HttpResp> {
        return rateProviderService.rate(this.stock.stockType, currency);
    }


}